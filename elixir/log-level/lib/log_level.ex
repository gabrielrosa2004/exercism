defmodule LogLevel do
  @labels [:trace, :debug, :info, :warning, :error, :fatal]
  @legacy_support [false, true, true, true, true, false]

  def to_label(level, legacy?) do
    cond do
      level >= length(@labels) or level < 0 -> :unknown
      Enum.at(@legacy_support, level) === legacy? -> Enum.at(@labels, level)
      Enum.at(@legacy_support, level) and not legacy? -> Enum.at(@labels, level)
      true -> :unknown
    end
  end

  def alert_recipient(level, legacy?) do
    cond do
      to_label(level, legacy?) in [:error, :fatal] -> :ops
      to_label(level, legacy?) == :unknown and legacy? -> :dev1
      to_label(level, legacy?) == :unknown and not legacy? -> :dev2
      true -> false
    end
  end
end
