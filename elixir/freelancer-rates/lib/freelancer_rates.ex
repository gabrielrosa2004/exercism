defmodule FreelancerRates do
  @daily_rate 8.0
  @month_billable_days 22

  def daily_rate(hourly_rate) do
    hourly_rate * @daily_rate
  end

  def apply_discount(before_discount, discount) do
    before_discount - before_discount * (discount / 100)
  end

  def monthly_rate(hourly_rate, discount) do
    worked_hours = @month_billable_days * @daily_rate
    price = worked_hours * hourly_rate
    price_with_discount = apply_discount(price, discount)
    trunc(Float.ceil(price_with_discount))
  end

  def days_in_budget(budget, hourly_rate, discount) do
    Float.floor(budget / apply_discount(daily_rate(hourly_rate), discount), 1)
  end
end
