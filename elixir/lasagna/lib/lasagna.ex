defmodule Lasagna do
  # Time, in minutes, to prepare a layer
  @time_to_prepare_layer 2

  def expected_minutes_in_oven(), do: 40

  def remaining_minutes_in_oven(minutes),
    do: expected_minutes_in_oven() - minutes

  def preparation_time_in_minutes(layers),
    do: @time_to_prepare_layer * layers

  def total_time_in_minutes(layers, minutes_in_oven),
    do: preparation_time_in_minutes(layers) + minutes_in_oven

  def alarm(), do: "Ding!"
end
